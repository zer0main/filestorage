package filestorage

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/starius/flock"
)

func (s *FileStorage) getLockPath() string {
	return filepath.Join(s.dir, "LOCK")
}

func (s *FileStorage) Lock(ctx context.Context) error {
	if s.lock != nil {
		return fmt.Errorf("already locked")
	}

	path := s.getLockPath()

	f, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("failed to open file %s for locking: %v", path, err)
	}

	if err := flock.LockFile(f); err != nil {
		return fmt.Errorf("failed to lock file %s: %v", path, err)
	}

	s.lock = f
	return nil
}

func (s *FileStorage) Unlock(ctx context.Context) error {
	if s.lock == nil {
		return fmt.Errorf("already not locked")
	}

	if err := flock.UnlockFile(s.lock); err != nil {
		path := s.getLockPath()
		return fmt.Errorf("failed to unlock file %s: %v", path, err)
	}

	s.lock = nil
	return nil
}
