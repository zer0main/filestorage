package filestorage

import (
	"context"
	"encoding/json"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFileStorageLoadMetaFromOffset(t *testing.T) {
	dirName := "test-tmp"

	err := os.Mkdir(dirName, os.ModePerm)
	require.NoError(t, err)

	t.Cleanup(func() {
		if err := os.RemoveAll(dirName); err != nil {
			t.Logf("remove all error: %v", err)
		}
	})

	storage := NewFileStorage(dirName)

	ctx := context.Background()

	type T struct {
		A int
		B string
	}
	input := []*T{
		{A: 1, B: "one"},
		{A: 2, B: "two"},
		{A: 3, B: "three"},
	}

	callback := func(w io.Writer) error {
		for _, v := range input {
			if err := json.NewEncoder(w).Encode(v); err != nil {
				return err
			}
		}
		return nil
	}

	err = storage.AppendMeta(ctx, callback)
	require.NoError(t, err)

	offsetCases := []int{0, 1, 2, 3}

	for _, offset := range offsetCases {
		events := []*T{}
		readerF := func(r io.Reader) error {
			decoder := json.NewDecoder(r)
			for {
				e := &T{}
				err := decoder.Decode(e)
				if err == io.EOF {
					break
				}
				if err != nil {
					return err
				}
				events = append(events, e)
			}
			return nil
		}

		err = storage.LoadMetaFromOffset(ctx, int64(offset), readerF)
		require.NoErrorf(t, err, "check offset: %d", offset)
		require.Equalf(t, input[offset:], events, "check offset: %d", offset)
	}
}
