package filestorage

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

func (s *FileStorage) getMetaPath() string {
	return filepath.Join(s.dir, "metadata.json")
}

func (s *FileStorage) LoadMetaFromOffset(ctx context.Context, offset int64, callback func(r io.Reader) error) error {
	path := s.getMetaPath()

	f, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			return callback(bytes.NewReader(nil))
		}
		return fmt.Errorf("failed to read %s: %w", path, err)
	}

	defer func() {
		if err := f.Close(); err != nil {
			log.Printf("failed to close file %s: %v", path, err)
		}
	}()

	r := bufio.NewReader(f)
	for i := int64(0); i < offset; i++ {
		if _, err := r.ReadBytes('\n'); err != nil {
			return fmt.Errorf("read line: %w", err)
		}
	}

	return callback(r)
}

func (s *FileStorage) LoadMeta(ctx context.Context, callback func(r io.Reader) error) error {
	path := s.getMetaPath()

	f, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			return callback(bytes.NewReader(nil))
		}
		return fmt.Errorf("failed to read %s: %w", path, err)
	}

	defer func() {
		if err := f.Close(); err != nil {
			log.Printf("failed to close file %s: %v", path, err)
		}
	}()

	return callback(f)
}

func (s *FileStorage) ReplaceMeta(ctx context.Context, callback func(w io.Writer) error) error {
	path := s.getMetaPath()

	// Write to temp file, sync and rename it to original.

	tmpPath := path + ".tmp"

	f, err := os.OpenFile(tmpPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return fmt.Errorf("failed to open file %s for writing: %w", tmpPath, err)
	}

	if err := callback(f); err != nil {
		if err := f.Close(); err != nil {
			log.Printf("failed to close file %s: %v", tmpPath, err)
		}
		return err
	}

	if err := f.Sync(); err != nil {
		return fmt.Errorf("flushing %s failed: %w", tmpPath, err)
	}

	if err := f.Close(); err != nil {
		return fmt.Errorf("closing %s failed: %w", tmpPath, err)
	}

	if err := os.Rename(tmpPath, path); err != nil {
		return fmt.Errorf("renaming %s to %s failed: %w", tmpPath, path, err)
	}

	return nil
}

func (s *FileStorage) AppendMeta(ctx context.Context, callback func(w io.Writer) error) error {
	path := s.getMetaPath()

	f, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("failed to open file %s for appending: %w", path, err)
	}

	w := &linesWriter{w: f}

	defer func() {
		if err := f.Close(); err != nil {
			log.Printf("failed to close file %s: %v", path, err)
		}
	}()

	if err := callback(w); err != nil {
		return fmt.Errorf("callback failed in AppendMeta: %w", err)
	}

	if err := f.Sync(); err != nil {
		return fmt.Errorf("flushing %s failed: %w", path, err)
	}

	return nil
}

type linesWriter struct {
	w io.Writer
}

func (w *linesWriter) Write(p []byte) (n int, err error) {
	newLineSuffix := []byte("\n")

	if !bytes.HasSuffix(p, newLineSuffix) {
		p = append(p, newLineSuffix...)
	}

	return w.w.Write(p)
}
