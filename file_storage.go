package filestorage

import (
	"os"
)

type FileStorage struct {
	dir  string
	lock *os.File
}

func NewFileStorage(dir string) *FileStorage {
	return &FileStorage{dir: dir}
}
